# Panty : template Scylla #

## Installation ##

* Téléchargez l'archive du thème dans l'onglet "Downloads" (même fonctionnement que pour le CMS).
* Dézippez le contenu du dossier dans votre dossier d'installation de Panty (les dossiers devraient se fusionner).
* Modifier le fichier de configuration de Panty. Changez la ligne : 

```
#!php

define('PANTY_THEME', 'ragefull'); // Thème du CMS
```

par

```
#!php

define('PANTY_THEME', 'scylla'); // Thème du CMS
```
